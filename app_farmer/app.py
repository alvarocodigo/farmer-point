from workspace import create_app
import sys 
import unittest

app = create_app()

@app.cli.command()
def test():
    tests  = unittest.TestLoader().discover('workspace',pattern='test_*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)

    if result.wasSuccessful():
        sys.exit(0)
    sys.exit(1)