from flask import Blueprint, request
from workspace import db 
from workspace.product.models import Product
from workspace.product.serializers import  product_schema


product_blueprint = Blueprint('product',__name__)


#agregar producto
@product_blueprint.route('/product', methods=['POST'])
def create():
    data = request.get_json()
    product = product_schema.load(data)

    db.session.add(product)
    db.session.commit()

    return product_schema.dump(product), 201

@product_blueprint.route('/product/<id>', methods=['PUT'])
def update(id):
    user = Product.query.filter_by(id = id).first()
    user = product_schema.load(request.get_json(), instance= user)
    

    db.session.add(user)
    db.session.commit()

    return product_schema.dump(user), 200
    

@product_blueprint.route('/register_farmer/<id>', methods=['PATCH'])
def patch(id):
    user = Product.query.filter_by(id = id).first()
    user = product_schema.load(request.get_json(), instance= user,partial=True)

    db.session.add(user)
    db.session.commit()

    return product_schema.dump(user), 200