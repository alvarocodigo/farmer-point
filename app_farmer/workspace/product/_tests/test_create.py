from flask_testing import TestCase
from flask import current_app
from workspace import db 
from workspace.product.models import Product

class TestCreateProduct(TestCase):
    def create_app(self):
        current_app.config.from_object('workspace.configs.TestingConfig')
        return current_app
    
    def setUp(self):
        db.create_all()
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_create(self):
        data = {
            "product_name":"Lechuga",
            "product_cost_":"23",
            "Product_history":"historia muy buena",
            "product_description":"Hola descroption"
        }
        self.assertEquals(0,Product.query.count())
        with self.client:
            response = self.client.post (
                '/product',
                json=data
            )
        self.assertStatus(response, 201)