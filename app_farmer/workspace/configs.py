class BaseConfig:
    SQLALCHEMY_DATABASE_URI = 'postgres://postgres:postgres@postgres-db:5432/dbfarmer'
    SQLALCHEMY_TRACK_MODIFICATIONS = False 

class DevelopmentConfig(BaseConfig):
    SQLALCHEMY_ECHO = True

class ProductionConfig(BaseConfig):
    SQLALCHEMY_ECHO = False 

class TestingConfig(BaseConfig):
    SQLALCHEMY_ECHO= False