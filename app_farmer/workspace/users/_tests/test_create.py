from flask_testing import TestCase
from flask import current_app
from workspace import db 
from workspace.users.models import Farmer_register
from workspace._tests.base import BaseTestCase

class TestCreateFarmer(BaseTestCase):
    def _call_create(self,data):
        with self.client:
            return self.client.post (
                '/resgister_farmer',
                json=data
            )

    def test_create(self):
        data = {
            "first_name":"Alvaro",
            "last_name":"Yañez",
            "email":"a@a.cl"
        }
        self.assertEquals(0,Farmer_register.query.count())
        response = self._call_create(data)
        self.assertStatus(response, 201)
        self.assertEquals(1,Farmer_register.query.count())
        json_response = response.json
        self.assertEquals(json_response['first_name'],data['first_name'])
        self.assertEquals(json_response['last_name'],data['last_name'])
        self.assertEquals(json_response['email'],data['email'])
    
    
    def test_create_without_firstname(self):
        data = {
            
            "last_name":"Yañez",
            "email":"a@a.cl"
        }

        with self.client:
            response = self.client.post(
                '/resgister_farmer',
                json = data
            )
        self.assertStatus(response, 400)
        json_response = response.json
        self.assertIn('first_name',json_response)
        self.assertEquals(json_response['first_name'][0],'Missing data for required field.')


    def test_create_without_lastname(self):
        data2 = {
            "first_name":"Alvaro",
            "email":"a@a.cl"
        }

        with self.client:
            response = self.client.post(
                '/resgister_farmer',
                json = data2
            )
        self.assertStatus(response, 400)
        json_response = response.json
        self.assertIn('last_name',json_response)
        self.assertEquals(json_response['last_name'][0],'Missing data for required field.')

    
    def test_create_without_email(self):
        data2 = {
            "first_name":"Alvaro",
            "last_name":"Yañez"
        }

        with self.client:
            response = self.client.post(
                '/resgister_farmer',
                json = data2
            )
        self.assertStatus(response, 400)
        json_response = response.json
        self.assertIn('email',json_response)
        self.assertEquals(json_response['email'][0],'Missing data for required field.')