from flask_testing import TestCase
from flask import current_app
from workspace import db 
from workspace.users.models import Farmer_register
from workspace._tests.base import BaseTestCase

class TestUpdateFarmer(BaseTestCase):

    def test_update(self):
        user = Farmer_register(first_name='alvarito',last_name='Yanez',email='A@c.l')

        db.session.add(user)
        db.session.commit()

        data = {
            "first_name":"Alvy ",
            "last_name":"Vargas",
            "email":"a@sa.cl"
        }
        self.assertEquals(1,Farmer_register.query.count())
        with self.client:
            response = self.client.put (
                f'/register_farmer/{user.id}',
                json=data
            )

        self.assertStatus(response, 200)
        self.assertEquals(1,Farmer_register.query.count())
        json_response = response.json
        self.assertEquals(json_response['first_name'],data['first_name'])
        self.assertEquals(json_response['last_name'],data['last_name'])
        self.assertEquals(json_response['email'],data['email'])
        updated_user = Farmer_register.query.filter_by(id=user.id).first()
        self.assertEquals(updated_user.first_name,data['first_name'])
        self.assertEquals(updated_user.last_name,data['last_name'])
        self.assertEquals(json_response['email'],data['email'])
    
    def test_update_unexist_user(self):
        data = {
            "first_name":"Alvy ",
            "last_name":"Vargas",
            "email":"a@sa.cl"
        }

        self.assertEquals(0,Farmer_register.query.count())
        with self.client:
            response = self.client.put (
                '/register_farmer/1',
                json=data
            )

        self.assertStatus(response, 404)
        self.assertEquals(0,Farmer_register.query.count())
       
    