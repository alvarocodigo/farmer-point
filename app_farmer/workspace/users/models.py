from workspace import db


class Farmer_register(db.Model):
    id =db.Column(db.Integer, primary_key=True, autoincrement=True)
    first_name = db.Column(db.String(20),nullable=False)
    last_name = db.Column(db.String(20),nullable=False)
    email = db.Column(db.String(20),nullable=False)
