from flask import Blueprint,request
from workspace2 import db 
from workspace2.users.models import User_register
from workspace2.users.serializers import registeruserschema

register_user_blueprint = Blueprint('register_users',__name__)


@register_user_blueprint.route('/register_users', methods=['POST'])
def create():
    data = request.get_json()
    user = registeruserschema.load(data)

    db.session.add(user)
    db.session.commit()

    return registeruserschema.dump(user), 201