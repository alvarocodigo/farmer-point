import React from 'react';
import NavBar from './NavBar'

function Register (){
    return (
        <div>
            <NavBar />
            <section>
                <div class="tarjeta">
                    <header>
                        <h3>Registro</h3>
                    </header>
                    <form>
                        <div>
                            <label>Correo</label>
                            <input type="email" placeholder="correo@ejemplo.cl" required />
                        </div>
                        <div>
                            <label>Contrase&ntilde;a</label>
                            <input id="password" type="password" required minlength="4" maxlength="8" />
                        </div>
                        <div>
                            <label>Confirmar Contrase&ntilde;a</label>
                            <input id="confirmacion" type="password" required minlength="4" maxlength="8" />
                        </div>
                        
                        <div>
                            <label>Edad</label>
                            <input type="number" min="1" max="150" step="any" required />
                        </div>
                        <div>
                            <label>Descripci&oacute;n</label>
                            <textarea rows="8">Texto ejemplo</textarea>
                        </div>
           
                        <button id="registrarse">Registrarse</button>
                    </form>
                </div>
            </section>
            <footer />
        </div>
    );
}
export default Register;